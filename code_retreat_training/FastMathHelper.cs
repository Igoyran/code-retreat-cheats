﻿using System;

namespace code_retreat_training
{
    public static class MathHelper_NoPlusMinus
    {
        public static double Add(double a, double b) => Math.Log(Math.Exp(a) * Math.Exp(b));
        public static double Sub(double a, double b) => Math.Log(Math.Exp(a) / Math.Exp(b));
    }

    public static class MathHelper_NoMultiplyDivide
    {
        public static double Mul(double a, double b) => Math.Exp(Math.Log(a) + Math.Log(b));
        public static double Div(double a, double b) => Math.Exp(Math.Log(a) - Math.Log(b));
    }

    //TODO: large integer numbers long add/sub/mul/div
}
