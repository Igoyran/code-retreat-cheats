﻿using System;
using NUnit.Framework;

namespace code_retreat_training
{
    [TestFixture]
    public static class FastMathHelper_Tests
    {
        private static Random rng = new Random();

        [Test]
        [Repeat(10)]
        public static void Test_SmallNumbers()
        {
            var a = (double)rng.Next(20);
            var b = (double)rng.Next(20);

            Assert.AreEqual(a + b, MathHelper_NoPlusMinus.Add(a, b), 1e-6, "Add");
            Assert.AreEqual(a - b, MathHelper_NoPlusMinus.Sub(a, b), 1e-6, "Subtract");
            Assert.AreEqual(a * b, MathHelper_NoMultiplyDivide.Mul(a, b), 1e-6, "Multiply");
            Assert.AreEqual(a / b, MathHelper_NoMultiplyDivide.Div(a, b), 1e-6, "Divide");
        }
    }
}
