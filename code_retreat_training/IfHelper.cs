﻿using System;
using System.Linq;

namespace code_retreat_training
{
    public class IfHelper_AsWhileOneTime
    {
        /// <summary>
        /// Условный оператор действует аналогично циклу while с максимальным числом итераций = 1.
        /// </summary>
        /// <param name="ifTrue">фрагмент кода, выполняющийся при истинности условия</param>
        /// <param name="predicate">условие</param>
        /// <param name="localVariables">вовлеченные в условие переменные</param>
        /// <returns></returns>
        public static object[] If(Func<object[], object[]> ifTrue, Func<object[], bool> predicate, object[] localVariables)
        {
            var resultVariables = localVariables;

            return WhileHelper_NoRecursion.infinite.TakeWhile(x => predicate(localVariables)).Select(x =>
            {
                resultVariables = ifTrue(localVariables);
                return resultVariables;
            }).DefaultIfEmpty(localVariables).First();
        }
    }
}
