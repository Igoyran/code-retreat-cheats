﻿using System;
using NUnit.Framework;

namespace code_retreat_training
{
    [TestFixture]
    public static class IfHelper_Tests
    {
        private static Func<object[], bool> TrueCondition_IncrementVariable_Predicate = localVariables =>
        {
            var x = (int)localVariables[0];
            var y = (int[])localVariables[1];

            return 2 < 3 || y.Length == 0;
        };

        private static Func<object[], object[]> IncrementVariable_Body = localVariables =>
        {
            var x = (int)localVariables[0];
            ++x;

            return new object[] { x };
        };

        private static Func<object[], bool> FalseCondition_IncrementVariable_Predicate = localVariables =>
        {
            var x = (int)localVariables[0];
            var y = (int[])localVariables[1];

            return x.GetType() == 0.GetType() && y.Length == 0;
        };

        [Test]
        public static void TrueCondition_IncrementVariable()
        {
            var x = 0;
            var y = new int[0];
            if (x < 3 || y.Length == 0)
                ++x;

            var localVariables = new object[] { 0, new int[0] };
            var result = IfHelper_AsWhileOneTime.If(IncrementVariable_Body,
                TrueCondition_IncrementVariable_Predicate, localVariables);

            Assert.AreEqual(x, (int)result[0]);
        }

        [Test]
        public static void FalseCondition()
        {
            var x = 5;
            var y = new int[10];
            if (x.GetType() == 0.GetType() && y.Length == 0)
                ++x;

            var localVariables = new object[] { 5, new int[10] };
            var result = IfHelper_AsWhileOneTime.If(IncrementVariable_Body,
                FalseCondition_IncrementVariable_Predicate, localVariables);

            Assert.AreEqual(x, (int)result[0]);
        }
    }
}
