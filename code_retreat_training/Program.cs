﻿using System;

namespace code_retreat_training
{
    /// <summary>
    /// Выводит простые числа от 2 до 100
    /// </summary>
    class Program
    {
        static Func<object[], bool> LessThan100 = localVariables =>
        {
            var i = (int)localVariables[0];
            return i < 100;
        };

        static Func<object[], object[]> ExternalPrimeSearchLoop = localVariables =>
        {
            var i = (int)localVariables[0];

            var internalPredicate = (Func<object[], bool>)localVariables[1];
            var internalLoop = (Func<object[], object[]>)localVariables[2];

            var externalIfBody = (Func<object[], object[]>)localVariables[3];
            var j = 2;

            var result = WhileHelper_NoRecursion.While(internalLoop, internalPredicate, new object[] { i, j, true });
            var isPrime = (bool)result[2];

            IfHelper_AsWhileOneTime.If(externalIfBody, _localVariables => (bool)_localVariables[0], new object[] { isPrime, i });

            return new object[] { ++i, internalPredicate, internalLoop, externalIfBody };
        };

        static Func<object[], object[]> PrintInteger = localVariables =>
        {
            // var isPrime = localVariables[0]
            var number = (int)localVariables[1];
            Console.Write($"{number} ");

            return localVariables;
        };

        static Func<object[], bool> LessThanSqrt = localVariables =>
        {
            var i = (int)localVariables[0];
            var j = (int)localVariables[1];
            var isPrime = (bool)localVariables[2];  // end loop if is not prime

            return j <= Math.Sqrt(i) && isPrime;
        };

        static Func<object[], object[]> InternalPrimeSearchLoop = localVariables =>
        {
            var i = (int)localVariables[0];
            var j = (int)localVariables[1];
            var isPrime = (bool)localVariables[2];

            var remainder = i - i / j * j;    // i % j
            isPrime = remainder != 0;
            j = (int)MathHelper_NoPlusMinus.Add(j, 1);

            return new object[] { i, j, isPrime };
        };

        static void Main(string[] args)
        {
            var i = 2;

            var result = WhileHelper_NoRecursion.While(ExternalPrimeSearchLoop, LessThan100,
                new object[] { i, LessThanSqrt, InternalPrimeSearchLoop, PrintInteger });
            Console.ReadKey();
        }
    }
}
