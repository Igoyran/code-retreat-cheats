﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace code_retreat_training
{
    public static class WhileHelper_NoRecursion
    {
        private class InfiniteEnumerator : IEnumerator<bool>
        {
            bool IEnumerator<bool>.Current => true;
            public object Current => true;
            public bool MoveNext() => true;
            public void Dispose() { }
            public void Reset() { }
        }

        private class InfiniteEnumerable : IEnumerable<bool>
        {
            public IEnumerator<bool> GetEnumerator() => new InfiniteEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => new InfiniteEnumerator();
        }

        public static readonly IEnumerable<bool> infinite = new InfiniteEnumerable();

        /// <summary>
        /// Реализует цикл while
        /// </summary>
        /// <param name="loopBody">тело цикла</param>
        /// <param name="predicate">условие цикла</param>
        /// <param name="firstLocalVariables">вовлеченные в цикл значения (до первой итерации)</param>
        /// <returns></returns>
        public static object[] While(Func<object[], object[]> loopBody, Func<object[], bool> predicate, object[] firstLocalVariables)
        {
            var parameters = firstLocalVariables;

            return infinite.TakeWhile(x => predicate(parameters)).Select(x =>
            {
                parameters = loopBody(parameters);
                return parameters;
            }).DefaultIfEmpty(firstLocalVariables).Last();
        }
    }

    public static class WhileHelper_Recursive_AndIf
    {
        public static object[] While(Func<object[], object[]> loopBody, Func<object[], bool> predicate, object[] currentLocalVariables)
        {
            if (predicate(currentLocalVariables))
            {
                var result = loopBody(currentLocalVariables);
                return While(loopBody, predicate, result);
            }
            return currentLocalVariables;
        }
    }
}
