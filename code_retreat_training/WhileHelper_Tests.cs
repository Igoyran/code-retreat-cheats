﻿using System;
using NUnit.Framework;

namespace code_retreat_training
{
    [TestFixture]
    public static class WhileHelper_Tests
    {
        private static Func<object[], bool> SumOfNumbersFromOneTo_CreatePredicate(int maxNumber)
        {
            return localVariables =>
            {
                var currentNumber = (int)localVariables[0];
                return currentNumber <= maxNumber;
            };
        }

        private static Func<object[], object[]> SumOfNumbersFromOneTo_Body = localVariables =>
        {
            var currentNumber = (int)localVariables[0];
            var sum = (int)localVariables[1];

            sum += currentNumber;
            ++currentNumber;

            return new object[] { currentNumber, sum };
        };

        [TestCase(0)]
        [TestCase(10)]
        public static void SumOfNumbersFromOneTo(int maxNumber)
        {
            var currentNumber = 1;
            var sum = 0;
            while (currentNumber <= maxNumber)
            {
                sum += currentNumber;
                ++currentNumber;
            }

            var predicate = SumOfNumbersFromOneTo_CreatePredicate(maxNumber);
            var result = WhileHelper_NoRecursion.While(SumOfNumbersFromOneTo_Body,
                predicate, new object[] { 1, 0 });
            var recursiveResult = WhileHelper_Recursive_AndIf.While(SumOfNumbersFromOneTo_Body,
                predicate, new object[] { 1, 0 });

            Assert.AreEqual(sum, (int)result[1], "No recursion");
            Assert.AreEqual(sum, (int)recursiveResult[1], "Recursive");
        }
    }
}
